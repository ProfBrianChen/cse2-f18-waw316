/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 9/19/2018 ////
/////////////////////

import java.util.Scanner;
//imports the Scanner class

public class Check {
//main method required for every Java program

  public static void main (String[] args) {
  //end of main method

    Scanner myScanner = new Scanner(System.in);
    //declaration of the Scanner object and call of the scanner instructor
    
    System.out.print("Enter the original cost of the check in the form $xx.xx: $ ");
    //asks the user for an input of an original check cost in a tens and hundreths form
    
    double checkCost = myScanner.nextDouble();
    //stores the user input as a double variable checkCost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    //asks a user for the percentage of the check they wish to tip for
    
    double tipPercent = myScanner.nextDouble();
    //stores the user input as a double variable tipPercent
    
    tipPercent /= 100;
    //converts the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    //asks the user to enter the amount of people in their party
    
    int numPeople = myScanner.nextInt();
    //store sthe user input as an integer variable numPeople
    
    double totalCost;
    //variable for the total cost of the dinner
    double costPerPerson;
    //variable for the cost of the dinner per person as a whole
    int dollars, //for storing digits
    dimes, pennies; //to the right of the decimal point
                    //for cost$
    totalCost = checkCost * (1 + tipPercent);
    //calculates the total cost of the the entire dinner, accommodating for the check cost and the tip
    costPerPerson = totalCost/numPeople;
    //calculates the cost per person
    dollars = (int) costPerPerson;
    //gets the whole amount, dropping decimal fraction
    dimes = (int) (costPerPerson * 10) % 10;
    //calculates the dimes each person owes
    pennies = (int) (costPerPerson * 100) % 10;
    //calculates the pennies each person owes
    
    System.out.println("Each person in the group owes $" +  dollars + "." + dimes + pennies);
    //prints the total amount each person owes
    
    
    
    
    
    
  }
}