# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a Bitbucket repository for all assginments concerned with CSE 002 (Introduction to Programming/Fundamentals of Java) at Lehigh University

### How do I get set up? ###

* ALL assignments will be pushed in the UNIX terminal with the following steps

  - //starting at workspace
  - cd cse2-f18-waw316
  - git add [folder of choice]
  - git commit -m '[Message of choice]'
  - git push
    - you will then be asked to enter your Bitbucket password
    - upon entering this, you will then see if the document was successfully pushed or not

### Who do I talk to concerning issues? ###

* Professor Brian Chen or Professor Sharon Kalafut
