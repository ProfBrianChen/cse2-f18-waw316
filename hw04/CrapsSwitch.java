/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 9/18/2018 ////
/////////////////////

import java.util.Scanner;
//imports the Scanner class

public class CrapsSwitch {
//main method required for every Java program

  public static void main (String[] args) {
  //end of main method
    
    Scanner myScanner = new Scanner(System.in);
    //declaration of the Scanner object and call of the scanner instructor
 
    
     System.out.print("Would you like to (0) randomly cast dice, or (1) pick your dice: ");
    //asks for input of which way to pick dice
     int dieChoice = myScanner.nextInt();
    //establishes an input as a choice of roll
    
    switch (dieChoice) {
      //if choice 0
      case 0:
           int die1 = (int) (5*(Math.random())+1);
            //random number for die1
           int die2 = (int) (5*(Math.random())+1);
            //random number for die2
           int sumDie = die1 + die2;
            //eval sum of two die
        
            switch (sumDie){
              case 2:
                System.out.println("Snake Eyes");
                //if the dice show 1s, prints snake eyes
                break;
              case 3:
                System.out.println("Ace Deuce");
                //if the dice show 2s, prints hard fours
                break;
              case 4:
                switch(die1) {
                  case 1:
                System.out.println("Easy Four");
                //easy 4
                    break;
                  case 2:
                System.out.println("Hard Four");
                  break;
                  case 3:
                System.out.println("Easy Four");
                //otherwise, easy 4
                  break;
                }
                break;
              case 5:
                System.out.println("Fever Five");
                //if the dice sum 5, prints fever 5                
                break;
              case 6:
                switch (die1) {
                  case 1:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6
                    break;
                  case 2:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6                 
                    break;
                  case 3:
                    System.out.println("Hard Six");
                     //if sum = 6 and die equal, hard 6
                    break;
                  case 4:
                    System.out.println("Easy Six");
                     //if sum = 6 and die not equal, easy 6                   
                    break;
                  case 5:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6                    
                    break;
                }
                  break;
                
              case 7:
                System.out.println("Seven out");
                //if sum = 7, seven out
                break;
                
              case 8:
                switch (die1) {
                  case 2:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 3:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 4:
                    System.out.println("Hard Eight");
                    //if sum 8 and dice even, hard eight
                    break;
                  case 5:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 6:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                } 
              break;
                
              case 9:
                System.out.println("Nine");
                //if sum 9, nine
                break;
              
              case 10:
                switch (die1) {
                  case 4:
                    System.out.println("Easy Ten");
                    //if sum 10 and dice not even, easy ten
                    break;
                  case 5:
                    System.out.println("Hard Ten");
                    //if sum 10 and dice even, hard ten
                    break;
                  case 6:
                    System.out.println("Easy Ten");
                    //if sum 10 and dice not even, easy ten
                    break;
                }
                break;
                
              case 11:
                System.out.println("Yo-leven");
                //if sum 11, yo-leven
                break;
                
              case 12:
                System.out.println("Boxcars");
                //if sum 12, boxcars
                break;
                
              default:
                System.out.println("Impossible rolls!");
                //cannot be possible if sum is less than 2 or exceeds 12
                }
        
      case 1:
      //if the die are manually casted
       System.out.print("Enter number on die 1: ");
       die1 = myScanner.nextInt();
      //input for die 1
      
       System.out.print("Enter number on die 2: ");
       die2 = myScanner.nextInt();
          //input for die 2
      
       sumDie = die1 + die2;
       //eval sum if dice
        
            switch (sumDie){
              case 2:
                System.out.println("Snake Eyes");
                //if the dice show 1s, prints snake eyes
                break;
              case 3:
                System.out.println("Ace Deuce");
                //if the dice show 2s, prints hard fours
                break;
              case 4:
                switch(die1) {
                  case 1:
                System.out.println("Easy Four");
                //easy 4
                    break;
                  case 2:
                System.out.println("Hard Four");
                  break;
                  case 3:
                System.out.println("Easy Four");
                //otherwise, easy 4
                  break;
                }
                break;
              case 5:
                System.out.println("Fever Five");
                //if the dice sum 5, prints fever 5                
                break;
              case 6:
                switch (die1) {
                  case 1:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6
                    break;
                  case 2:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6                 
                    break;
                  case 3:
                    System.out.println("Hard Six");
                     //if sum = 6 and die equal, hard 6
                    break;
                  case 4:
                    System.out.println("Easy Six");
                     //if sum = 6 and die not equal, easy 6                   
                    break;
                  case 5:
                    System.out.println("Easy Six");
                    //if sum = 6 and die not equal, easy 6                    
                    break;
                }
                  break;
                
              case 7:
                System.out.println("Seven out");
                //if sum = 7, seven out
                break;
                
              case 8:
                switch (die1) {
                  case 2:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 3:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 4:
                    System.out.println("Hard Eight");
                    //if sum 8 and dice even, hard eight
                    break;
                  case 5:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                  case 6:
                    System.out.println("Easy Eight");
                    //if sum 8 and dice not even, easy eight
                    break;
                } 
              break;
                
              case 9:
                System.out.println("Nine");
                //if sum 9, nine
                break;
              
              case 10:
                switch (die1) {
                  case 4:
                    System.out.println("Easy Ten");
                    //if sum 10 and dice not even, easy ten
                    break;
                  case 5:
                    System.out.println("Hard Ten");
                    //if sum 10 and dice even, hard ten
                    break;
                  case 6:
                    System.out.println("Easy Ten");
                    //if sum 10 and dice not even, easy ten
                    break;
                }
                break;
                
              case 11:
                System.out.println("Yo-leven");
                //if sum 11, yo-leven
                break;
                
              case 12:
                System.out.println("Boxcars");
                //if sum 12, boxcars
                break;
                
              default:
                System.out.println("Impossible rolls!");
                //if sum not 2 to 12, it cannot be rolled
                }
              
            }
          
  }
}
  