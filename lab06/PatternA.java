/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 10/17/2018 ///
/////////////////////

import java.lang.Math;
import java.util.Scanner;

public class PatternA {
  public static void main(String args[]) { //class that all programs need
    
    Scanner scr = new Scanner(System.in); //introduces the scanner scr into the program
    String junk; //junk cleaner string
    int dim; //input for the pyramid height
    int i = 0; //initial counter i
    int j = 0; //initial counter j
   
    System.out.println("What dimension (height), between 1 and 10, do you want your pyramid as?:"); //asks for a dimension for the constructed pyramid
    while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    }
    
    dim = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
    
    while (dim < 1 || dim > 10) { //while the var dim falls outside our desired range
      
      System.out.println("Integer falls out of range, re-enter: "); //asks for an integer within range
          while (!scr.hasNextInt()) {//so long as the next value is not an integer
      
      //System.out.println("Non-integer. Re-enter: "); //asks again for an integer
      junk = scr.nextLine(); //junk string
    }
    
      dim = scr.nextInt(); //takes the proper input and makes it dim

    }
    for (i = 1; i <= dim; i++) { //row sequencer
        
        for (j = 1; j <= i; j++) { //organizes the within parts of the pyramid

          System.out.print(j + " "); //prints the components of the pyramid
    }
      System.out.println(); //prints an extra line to space out the pyramid
    }
   
  }
}
