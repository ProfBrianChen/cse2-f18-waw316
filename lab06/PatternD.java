/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 10/17/2018 ///
/////////////////////

import java.lang.Math;
import java.util.Scanner;

public class PatternD {
  public static void main(String args[]) { //class that all programs need
    
    Scanner scr = new Scanner(System.in); //introduces the scanner scr into the program
    String junk; //junk cleaner string
    int dim; //input for the pyramid height
    int i = 0; //initial counter i
    int j = 0; //initial counter j
    int reset = 0;
    

    System.out.println("What dimension (height), between 1 and 10, do you want your pyramid as?:"); //asks for a dimension for the constructed pyramid
    while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    }
    
    dim = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
    
    while (dim < 1 || dim > 10) { //while the var dim falls outside our desired range
      
      System.out.println("Integer falls out of range, re-enter: "); //asks for an integer within range
          while (!scr.hasNextInt()) {//so long as the next value is not an integer
      
      System.out.println("Non-integer. Re-enter: "); //asks again for an integer
      junk = scr.nextLine(); //junk string
    }
    
      dim = scr.nextInt(); //takes the proper input and makes it dim

    }

    System.out.println("Pyramid height: " + dim); //what the height will be
    
    for (i = dim; i >= 1; i--) { //counts rows from dim down to 1
 
        for (j = i; j >= 1; j--) { //puts numbers in pyramid accordingly
          System.out.print(j + " "); //spaces numbers in pyramid accordingly
    }
      System.out.println(); //formats pyramid
    }
   
  }
  }

