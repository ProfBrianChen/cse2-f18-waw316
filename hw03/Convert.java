/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 9/18/2018 ////
/////////////////////

import java.util.Scanner;
//imports the Scanner class

public class Convert {
//main method required for every Java program

  public static void main (String[] args) {
  //end of main method
    
    Scanner myScanner = new Scanner(System.in);
    //declaration of the Scanner object and call of the scanner instructor
    
    double acresLand;
    //variable for acres of land affected
    double averageRainfall;
    //variable for average rainfall in inches
    
    double convertAcrestoMiles = 43560;
    //conversion variable for converting acres to miles
    
    System.out.print("Enter the affected area in acres: ");
      //asks for user input of acres
    acresLand = myScanner.nextDouble();
      //stores input of acres as acresLand
    
    System.out.print("Enter the rainfall in the affected area in inches: ");
      //asks for user input of acres
    averageRainfall = myScanner.nextDouble();
      //stores input of rainfall as averageRainfall
    
    double acresNew = acresLand * convertAcrestoMiles;
    //converts acres entered to a new format that will allow for proper conversion
    
    double acreInches = acresLand * averageRainfall;
    //converts acres and inches of rain to acre-inches

    double acreInchtoGallon = 27154.2857;
    //conversion factor from acre-inches to gallons
    
    double gallonsWater = acreInches * acreInchtoGallon;
    //converts acre-inches of water into gallons
    
    double gallonsToCubicMiles = 0.000000000000908169;
    //conversion factor from gallons to cubic miles
    
    double cubicMilesRain = gallonsWater * gallonsToCubicMiles;
    //calculates the amount of rain in cubic miles
    
    System.out.println(cubicMilesRain + " cubic miles of rain");
    //prints out a statement showing how much rain fell in cubic miles
  
  }
}
  