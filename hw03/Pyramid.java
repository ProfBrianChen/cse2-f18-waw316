/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 9/18/2018 ////
/////////////////////

import java.util.Scanner;
//imports the Scanner class

public class Pyramid {
//main method required for every Java program

  public static void main (String[] args) {
  //end of main method
    
    Scanner myScanner = new Scanner(System.in);
    //declaration of the Scanner object and call of the scanner instructor
     System.out.print("Side of the square base of the pyramid: ");
    //asks for input of the side of the base square
     double squareSide = myScanner.nextDouble();
    //establishes an input as a dimension of the square's side in a pyramid's base
    
     System.out.print("Height of the pyramid: ");
    //asks for a user input of the height of a pyramid
     double heightPyramid = myScanner.nextDouble();
    //establishes an input as the height of a pyramid
    
     double squareArea = (squareSide)*(squareSide);
    //calculates area of the base of the pyramid
    
     double volumePyramid = ((squareArea)*(heightPyramid)/3);
    //calculates the volume of the pyramid
    
    System.out.println("The volume of the pyramid is " + volumePyramid);
    //prints out the volume of the pyramid
    
  }
}
  