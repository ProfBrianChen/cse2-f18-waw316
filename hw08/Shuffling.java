a/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 11/13/2018 ///
/////////////////////

import java.util.Scanner;
public class Shuffling {  //main method
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); //scanner intro
 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    

//rank names
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 

String[] cards = new String[52]; //string for all cards
String[] hand = new String[5]; //string for our hand
int numCards = 5; //number of cards in hand
int again = 1; //repetition counter, as provided on input
int index = 51; //index to count what random cards we want for our hand
for (int i=0; i<52; i++){ //for loop to count our cards
  cards[i]=rankNames[i%13]+suitNames[i/13]; //establishes our card
  System.out.print(cards[i]+" "); //prints the card
} 
System.out.println();
printArray(cards); //prints the array of our cards
shuffle(cards); //method to shuffle our cards
printArray(cards); //this will print our shuffled cards
while(again == 1){ //a while statement to keep us in a position to keep getting hands
   hand = getHand(cards,index,numCards); //gets our hand
   printArray(hand); //prints our hand
   index = index-numCards; //takes the hand away from the deck so we can get a new hand
   System.out.println("Enter a 1 if you want another hand drawn"); //asks user for an input to continue
   again = scan.nextInt(); //scanner takes user's integer input
}  
  }

public static void printArray(String [] cards) { //method to print an array for the card array brought in
  
  for (int i = 0; i < cards.length; i++) { //for loop to count the cards
  System.out.print(cards[i] + " "); //prints the cards
  
  }
  System.out.println();
  System.out.println();
}

public static void shuffle (String [] cards) { //method to shuffle the cards
  int [] rand = new int [52]; //sets a new array to set the random numbers as
  String temp; //temporary save variable
  
  for (int randNum = 0; randNum < 52; randNum++) {//counts the card positions in the array
        int k = (int) (Math.random()*51); //generates a random number from 0 to 51
        temp = cards[randNum]; //sets the temporary string as the intial cards value while we shuffle
        cards[randNum] = cards[k]; //switches the original card component with the new random card
        cards[k] = temp; //sets the random number as a component of the random array            
  }

}
public static String [] getHand (String [] cards, int i, int y) { //method used to get the hand
  String [] hand = new String [y]; //new array of size based on numHands to get the hand
  
  for (int k = 0; k < hand.length ;k++) { //for the length of the hand, we will get new cards for the size of the hand
    hand[k] = cards[i-k]; //sets the array hand equal to the specific random cards
  }
  return hand;//returns the hand
}

}