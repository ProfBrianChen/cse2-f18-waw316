////////////
//// CSE 02 WelcomeClass
///  Wade Warner
///  Section 210
///  9/4/2018

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints an expressional LU ID output taking up 8 lines of code to terminal window
   
    System.out.println("    ----------- ");
    //prints out the top component of the "WELCOME" frame
    System.out.println("    | WELCOME |");
    //prints out the middle component of the "WELCOME" frame
    System.out.println("    ----------- ");
    //prints out the bottom component of the "WELCOME" frame
    System.out.println("  ^  ^  ^  ^  ^  ^");
    //prints out the top arrows of the ID
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    //prints out the top arrow components
    System.out.println("<-W--A--W--3--1--6->");
    //prints out the ID
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    //prints out the bottom arrow components
    System.out.println("  v  v  v  v  v  v");
    //prints out the bottom arrows of the ID
    System.out.println(" My name is Wade, and I am an ISE major at Lehigh University.");
    //prints out a Twitter style biography about me
      }
}