Homework 6 EncryptedX

Total Score: 94

Compiles                                                20/20 pts

Comments                                                10/10 pts
    Nice comments!

Uses nested for loop properly                           15/15 pts

Checks for between 0-100                                5/5 pts
    
Program works  (shows some sort of grid-like output)    50/50 pts

Asks for 1-100 instead of 0-100                         -6 pts
