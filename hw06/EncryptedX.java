/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 10/23/2018 ///
/////////////////////

import java.lang.Math;
import java.util.Scanner;

public class EncryptedX {
  public static void main(String args[]) { //class that all programs need
    Scanner scr = new Scanner(System.in); //introduces the scanner scr into the program
    String junk; //junk cleaner string
    int box; //input for the pyramid height
    int i = 0; //initial counter i
    int j = 0; //initial counter j
    

    System.out.println("What box dimension (per side), between 1 and 100, do you want your box as?:"); //asks for a dimension for the constructed box
    while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    }
    
    box = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
    
    while (box < 1 || box > 100) { //while the var dim falls outside our desired range
      
      System.out.println("Integer falls out of range, re-enter: "); //asks for an integer within range
          while (!scr.hasNextInt()) {//so long as the next value is not an integer
      
      System.out.println("Non-integer. Re-enter: "); //asks again for an integer
      junk = scr.nextLine(); //junk string
    }
    
      box = scr.nextInt(); //takes the proper input and makes it box

    }

    System.out.println("Box dimensions: " + box + " x " + box); //what the box dimensions will be
    
    for (i = 1; i <= box; i++) { //counts rows from dim down to 1
      
        for (j = 1; j <= box; j++) { //puts characters in box accordingly
          if (j == i || j == (box + 1) - i) {//allots space entry for according distance to form x
          System.out.print(" "); //spaces numbers in box accordingly
    }
          else {
          System.out.print("*"); //puts asterik in spot accordingly
          }
          
      
    }
   System.out.println(); //formats box and x
  }
  }}

