////////////
//// CSE 02 Cyclometer
///  Wade Warner
///  Section 210
//   9/12/2018
public class Cyclometer{
  
  // main method required for every Java program
  public static void main(String args[]){
    
    // our input data
    int secsTrip1 = 480; //time of the first trip
    int secsTrip2 = 3220; //time of the second trip
    int countsTrip1 = 1561; //number of rotations for the first trip
    int countsTrip2 = 9037; //number of rotations for the second trip
    
    //intermediate variables and output data
    double wheelDiameter = 27.0; //bicycle wheel's diameter
    double PI = 3.14159; //approximation for the math. value of pi
    double feetPerMile = 5280; //number of feet per mile
    double inchesPerFoot = 12; //number of inches in a foot
    double secondsPerMinute = 60; //number of seconds per minute
    double distanceTrip1, distanceTrip2, totalDistance; //evaluative variables for distances
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); //travel info for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //travel info for trip 2
    
    distanceTrip1 = countsTrip1*wheelDiameter*PI;//calculates total distance for trip 1
    //above gives distance in inches
    //for each count, a rotation of the wheel travels the diameter in inches * PI
    distanceTrip1/= inchesPerFoot * feetPerMile; //gives distace in miles
        
    distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//calculates total distance for trip 2
    //above gives distance in inches
    //for each count, a rotation of the wheel travels the diameter in inches * PI
    totalDistance = distanceTrip1 + distanceTrip2; //total distance of both trips
    
    System.out.println("Trip 1 was " + distanceTrip1 + " miles."); //prints statement about Trip 1 distance
    System.out.println("Trip 2 was " + distanceTrip2 + " miles."); //prints statement about Trip 2 distance
    System.out.println("The total distance was " + totalDistance + " miles."); //prints statement about total trip distanceTrip2
    
    //END CODE
    
  } //end of main method public static void main String args
  
} //end of class