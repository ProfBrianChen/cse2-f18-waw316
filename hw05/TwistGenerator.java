
/////////////////////
////// CSE 2 ////////
////// WADE WARNER //
////// 10/9/2018 ////
/////////////////////

import java.lang.Math;
import java.util.Scanner;

public class TwistGenerator {
  public static void main(String args[]) {
    Scanner scr = new Scanner(System.in);
   
    String junk; //junk cleaner string

    int length; //integer for length
    
    System.out.println("Enter the length of a twist: "); //asks for the length of a twist

    
      while (!scr.hasNextInt()){ //waits until an integer is entered
        
        System.out.println("Error: Re-input correctly"); //error print

        junk = scr.nextLine();//junk string

      }
    length = scr.nextInt();//inputs the input length as the length of the twist
    junk = scr.nextLine();//clears for next input
    System.out.println("Length of the twist = " + length); //prints out the length of the twist

    int count; //introduces a counter

    count = length; //sets the counter equal to the length
    int reg = 2; //the integer that sets regulation so that the while loops run so the twist comes out correctly

  while(count >= 1 && reg == 2){ //while the counter is greater than or = to 1 and when the regulator is at the initial value
    System.out.print("\\"); //first part of twist print
    count = count - 1; //count loses value by 1
    reg = 1; //sets new regulation value

    while (count >= 1 && reg == 1) {//while count is >= 1 and when the regulator is 1
    System.out.print(" ");//prints the space needed for the twist
    count = count - 1; //count loses 1
    reg = 0;//sets new regulation value

      while (count >= 1 && reg == 0){
    System.out.print("/"); //part of twist print
    count = count-1; //count loses value by 1
    reg = 2; //sets reg value to initial to re-run loops
  }}}

  System.out.println();//allows for the second part of the twist to print correctly
 
  reg = 2; //resets reg value from prior loops to assure correct running
  count = length; //sets count back to length dimension
  while(count >= 1 && reg == 2){//this loop counts down to the length to get the right components
    System.out.print(" ");//prints a required space for the twist
    count = count - 1; //subtracts 1 from counter
    reg = 1; //sets new reg value

    while (count >= 1 && reg == 1) { 
    System.out.print("x"); //x component of string
    count = count - 1; //counts down by 1
    reg = 0;//sets new reg value

      while (count >=1 && reg == 0){
    System.out.print(" "); //last space
    count = count - 1;//counts down by 1
    reg = 2;//sets reg value to re-loop
  }}}

  System.out.println();//allows for the final part of the twist
  reg = 2; //sets initial reg value to run these loops
  count = length;//sets the count back equal to the length so a similar set up to the first set of loops can be run

    while(count >= 1 && reg == 2){
    System.out.print("/");//prints first component of the last part of the twist
    count = count - 1;//lowers the counter by 1
    reg = 1;//sets new value for reg

    while (count >= 1 && reg == 1) {
    System.out.print(" ");//prints a component of the twist
    count = count - 1;//lowers the counter by 1
    reg = 0;//changes value for reg

      while (count >= 1 && reg == 0){
    System.out.print("\\"); //part of the twist
    count = count-1; //counter drops by 1
    reg = 2; //changes reg
  }}}

      System.out.println();
    //prints an xtra line
  
  
  }

}