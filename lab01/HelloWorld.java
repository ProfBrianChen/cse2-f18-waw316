////////////
//// CSE 02 HelloWorld
///  Wade Warner
///  Section 210
//   9/5/2018
public class HelloWorld{
  
  public static void main(String args[]){
    ///prints Hello, World to terminal window
    System.out.println("Hello, World");
  }
}