//////////////////////
/////Wade Warner//////
////11-27-2018////////
//////////////////////

import java.util.Scanner;
import java.lang.Math;
import java.util.Random;
//imports all above

public class CSE2Linear { //MAIN method
  
  public static void main (String [] args) {
    
    int [] scores = new int [15]; //initial array for scores
    int [] shuffleScores = new int [scores.length]; //array for storing shuffled scores
    Scanner scr =  new Scanner (System.in); //establishes scanner
    String junk; //junk string
          
    System.out.println("Enter 15 final grades for CSE2 in ascending order: "); //asks for 15 grades
    
    for (int i = 0; i < scores.length; i++) {
      while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    
      }

    scores[i] = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
      
    
    while (scores[i] > 100 || scores[i] < 0) { //if not in score range
      System.out.println("Ineligible grade: Re-enter"); //prompts the user to re-enter an eligible grade
         scores[i] = scr.nextInt(); //reads the next integer
    }
    if (i >= 1) { //so long as the index is greater than 1, otherwise, there will be a runtime error
      while (scores[i] < scores[i-1]){ //if the entered score is less than the last
      System.out.println("Non-ascending order: Re-enter"); //prompts the user to enter a score that is greater than or equal to last
      scores[i] = scr.nextInt(); //reads the next integer
    }
    }
}
    for (int j = 0; j < scores.length; j++) { //for the amount of scores
      System.out.print(scores[j] + " "); //prints out the scores
    }
    System.out.println(); //line for formatting
    
    System.out.println("Score to search (binary): "); //asks for a search value
    
    while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    
      }

    int scoreFind = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
    
    binarySearch(scores,scoreFind); //runs the method to search with a binary search
         
    shuffleScores = scramble(scores); //method that shuffles the scores
    
    System.out.println();
    System.out.println("Score to search (linear): "); //asks for a score to search in a linear fashion
    
     while (!scr.hasNextInt()) { //holds until integer is put in
      
       System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
       junk = scr.nextLine(); //junk string
    
     }

   scoreFind = scr.nextInt(); //reads for the next integer
   junk = scr.nextLine(); //junk string
    
    linearSearch(shuffleScores,scoreFind); //method used to search with a linear search
}
  
  public static void binarySearch(int [] scores, int searchScore) { //method for binary search
      int mid; //middle value index
      int low; //low value index
      int high; //high value index
      int iterate = 0; //establishes a counter that counts iterations
      int found = -1; //found or not, -1 means no, 1 means yes
      low = 0; //establishes the low value index as the lowest value in the sequence
      high = scores.length-1; //sets the high value index as the highest value in the sequence

      while (high >= low) { //as high is greater than low
         mid = (high + low) / 2; //finds the middle value based on what has been already read
         if (scores[mid] < searchScore) { //if the middle value is less than the score being searched for
            low = mid + 1; // increases the low index value to the middle index + 1
            iterate += 1; //counts for an iteration
                     } 
         else if (scores[mid] > searchScore) { //if the middle value is greater than the score being searched for
            high = mid - 1; //decreases the high index value to mid and an additional 1
            iterate += 1; //counts for an iteration
                     } 
         else {
           high = -1; //sets the high index value as lower than low index value to stop while loop
           found = 1; //the search value has been found
           iterate += 1;
             }
        }

      if (found == -1) { //if the search value has not been found
        System.out.println(searchScore + " was not found."); //states the search value has not been found
      }
      
      else { //otherwise, the search value has been found
        System.out.println(searchScore + " was found after " + iterate + " iterations.");//states the search value has been found after a certain number of iterations
      }
   }
  
  public static int [] scramble (int [] scores) { //scramble method
  
    Random rnd = new Random (); //random introduction
    System.out.println("Shuffled"); //mentions that this will be a shuffled array
    for(int k = scores.length - 1; k > 0; k--) { //for the values of the array
      
      int newIndex = rnd.nextInt(k); //index as a random number
      int hold = scores[newIndex]; //holds the value of the original array at the random index
      scores[newIndex] = scores[k]; //stores value as part of new array
      scores[k] = hold; //stores value as part of new array
      
      
      System.out.print(scores[k] + " "); //prints shuffled array
     
  }
    return scores; //returns the shuffled array for linearSearch method
  }
  
  
  public static void linearSearch(int [] scores, int findScore) { //method for linear search
    int iterate = 0; //establishes iterate
    int found = -1; //found variable, as before
   
    for(int m = scores.length - 1; m > 0; m--) {
      iterate += 1; //iterates every time the search value is not found
      if (scores[m] == findScore) {
        found = 1; //value is found
        break; //breaks out of for loop
    }
    }
    
    if (found == -1) { //not found
      System.out.println(findScore + " was not found."); //prints that the value has not been found
    }
    else { //found
      System.out.println(findScore+ " was found after " + iterate + " iterations."); //prints that the value has been found after a number of iterations
    }
  }
}
