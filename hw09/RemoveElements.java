//////////////////////////
//////////////////////////
//////Wade Warner/////////
////////11-27-2018////////
//////////////////////////

import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static int [] randomInput () { //method for creating an array with random inputs
   int [] randomArray = new int [10]; //establishes a new array randomArray for random inputs
   for(int i = 0; i < randomArray.length; i++) { //for the values of the array
      randomArray[i] = (int) (java.lang.Math.random()*10); //each value of the array gets a random number 0-9, with replacement
     
      System.out.print(randomArray[i] + " "); //prints shuffled array
   }
      System.out.println(); //prints an extra line for spacing purposes
   return randomArray; //returns the random array
  }
  
  public static int [] delete (int [] list, int pos) { //method for deleting the element at a single index
  
    int [] modDelete = new int [10-1]; //makes a new array to accomodate for the loss of one element
    if (pos > 9 || pos < 0) { //if the index is not valid, or not betweeen 0-9
     System.out.println("The index is not valid"); //prints that the index is not valid
    }
    else {System.out.println("The index has been found.");} //if the index fits the range, the index is valid
    
    for (int i = 0; i < list.length; i++) { //for the amount of indices
    
      if (i == pos) { //if the index is of the position
         for (int j = i; j < list.length - 1; j++) { //creates the new modDelete array based on what comes after the deleted element
          modDelete[j] = list[j+1]; //sets the new array equal to all values beyond the deleted element
        }
        break;//breaks out of the after modDelete after element loop
      }
      else {modDelete[i] = list[i];} //fills in otherwise
    }
    
    System.out.println("Element with index " + pos + " has been removed."); //prints that the element at index pos has been removed
  return modDelete;//returns the new array
      
    }
  
  public static int [] remove (int [] list, int target) { //method to delete elements from an array

    int count = 0; //count variable to count the number of times an element occurs in a given array
    
    for (int i = 0; i < list.length; i++) { //for the length of the array list, with our random variables
      
      if (list[i] == target) { //if an element in our array is equal to our provided element
      
        count++; //increment the counter by 1
      
      }
    }
      if (count > 0) { //if there has been an element counted
        System.out.println("The element has been found."); //prints that the element has been found
      }
      else {System.out.println("The element has not been found.");} //prints that the element has not been found
  
   int [] modRemove = new int [list.length - count]; //makes a new array modRemove that takes the dimension of all the remaining elements beyond what we are looking for, as determined above
   int k = 0; //k counter for modRemove index
      
   for (int j = 0; j < list.length; j++)  { //for the length of the list
     
     if (list[j] != target) { //if the member of the list is not the target value
           modRemove[k] = list[j]; //place that value of list into the modRemove array
           k++; //increment to move to the next index of modRemove
           }
     else {continue;} //if the target value is in array list, move to the next iteration of the loop
       }
     
  return modRemove; //returns the array with all the target elements erased
  }
  
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
  }