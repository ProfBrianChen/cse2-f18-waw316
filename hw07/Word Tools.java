//////////////////////
////////WADE WARNER///
///////10-31-2018/////
//////////////////////


import java.util.Scanner;//import scanner

public class TextEditor{ //main class
  public static void main(String [] args){ //main method
    Scanner input = new Scanner(System.in); //brings a scanner input into the program
    char choice; //our character of choice
    String junk; //a junk string for the input of our word to find
    String sampleTxt; //our sample text
    int charactersNWS; //int representing characters that arent whitespace
    int numWords; //int representing words in sample text
    String wordToFind; //our string as the word or phrase we want to find
    int findWord; //int representing the number of things we wanted to find that are found
    String newExLessTxt; //the sample text w/o exclamation points
    String newSpaceLessTxt;//the sample text w/o double spaces
    
    System.out.println("Enter your text: "); //asks user for text
    sampleTxt = sampleText(input); //fills in a string with the return of method sampleText, which takes user input
    System.out.println(sampleTxt); //prints out the user text

    do{ //dowhile
      printMenu(); //runs the menu method
      choice = getChar(input); //the choice selected as done by a method that allows for a character to be selected
      switch (choice){ //switch that operates as each option is selected
        case 'c': //for c chosen
                charactersNWS = getNumOfNonWSCharacters(sampleTxt); //runs method of characters - whitespace
                System.out.println("Characters: " + charactersNWS); //prints out the number of characters in the text provided
                break; 
        case 'w': //w chosen
                numWords = getNumOfWords(sampleTxt); //method to get # of words in text
                System.out.println("Number of words: " + numWords);//prints the number of words in the text
                break;                
        case 'f': //f chosen
                System.out.println("Enter a word or phrase to be found: "); //asks the user to enter a word to be found
                input.nextLine(); //clearance of buffer
                wordToFind = input.nextLine(); //stores the input as wordToFind
                findWord = findText(wordToFind, sampleTxt); //runs a method that calculates the amt of times the input appears in the text
                System.out.println(wordToFind + " found " + findWord + " times"); //prints out the #
                break;
        case 'r': //r chosen
                newExLessTxt = replaceExclamation(sampleTxt); //method that replaces all ! in text with .
                System.out.println("Edited text: " + newExLessTxt); //prints out the new text
                break;
        case 's': //s chosen
                newSpaceLessTxt = shortenSpace(sampleTxt); //method that replaces all double spaces with single spaces
                System.out.println("Edited text: " + newSpaceLessTxt); //prints out the new text
                break;
        case 'q': System.out.println("Goodbye"); //q chosen, which exits the menu
                break;                
        default: System.out.println("you entered an invalid value -- try again"); //invalid if the char does not fit
                break;
      }
    }while(choice != 'q'); //all above done while q is not chosen
  }
  
  public static String sampleText(Scanner input) { //sample text method
    String stringInput; //string for input
    
    stringInput = input.nextLine(); //our input, as asked for in the main method

    return stringInput; //returns the string
    
  }    
  
  public static void printMenu(){ //method that simply prints the options menu
  
      System.out.println("MENU");
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !'s");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      System.out.println(" ");
      System.out.println("Choose an option:");
    
  }
  
  public static char getChar (Scanner input) { //method that gets a character as input by user
   
    char choose; //var for the chosen character
    
    choose = input.next().charAt(0); //the chosen first char from a string (any char entered as an input is a string, this converts)
    
    return choose; //returns the selected char
    
  }
  
  public static int getNumOfNonWSCharacters (String samp) { //method to find the # of chars
    
    int i; //i variable for for loop
    int m; //return variable
    int lengthStr; //variable for the length of the string
    int spaces = 0; //# of white spaces
    
    lengthStr = samp.length(); //the length of the string
    
    for (i = 0; i < lengthStr; i++) { //when i is less than or equal to the length of the string
      
      if (samp.charAt(i) == ' ') { //if the character i of the string is WS
                spaces += 1; //increase spaces by 1
      }
    }
    
    m = lengthStr - spaces; //the length of the string without the WS is the # of characters
    
    return m; //returns this value    
    
   }
  public static int getNumOfWords(String samp) { //method for finding the # of words
    int i = 0; //i var for for loop
    int n; //return var
    int lengthStr; //length of string
    int j = 0; //secondary counter variable
    
    lengthStr = samp.length(); //length of string
    
    for (i = 0; i < lengthStr; i++) { //for i less than or equivalent to the string's length
      char pres = samp.charAt(i); //character stored as a character
      if (pres == ' ') { //if there is white space
          j += 1; //account for a space
          if (samp.charAt(i+1) == ' ') { //when there is another white space after the one before
            j -= 1; //negates the extra whitespace
          }}
              }
    
    n = j+1; //number of words, plus 1 to accommodate for the last word, which has no space after it
  
    return n; //returns the value
      }
  public static int findText(String junk, String samp) {//method for finding the quantity of something in the input string
    
   int p; //integer to represent the found objects
   p = samp.split(junk).length - 1; //splits the string when the junk (word to be found) string is observed
      
   return p; //returns the number of times this occurrence happened
  }
  
  public static String replaceExclamation(String samp) { //method for replacing the !s
    
    String newTxt; //new text string
    
    newTxt = samp.replace("!","."); //replaces ! with . in the string
    
    return newTxt; //returns the updated text
       
      }
  public static String shortenSpace(String samp) { //replaces "  " with  " "
   
    String newTxt2; //new text string
    
    newTxt2 = samp.replace("  "," "); //replaces " " with " "
    
    return newTxt2; //returns the updated text
       
      }
    }
