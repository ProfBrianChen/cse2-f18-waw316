//////////////////////
//// CSE 02 Arithmetic
///  Wade Warner
///  Section 210
//   9/11/2018
//////////////////////

public class Arithmetic {
      
      // main method required for every Java program
        public static void main(String args []){
        
        int numPants = 3;
          //number of pairs of pants
        double pantsPrice = 34.98;
          //cost per pair of pants
        int numShirts = 2;
          //number of sweatshirts
        double shirtPrice = 24.99;
          //cost per shirt
        int numBelts = 1;
          //number of belts
        double beltCost = 33.99;
          //cost per belt
        double paSalesTax = 0.06;
          //the Pennsylvania tax rate
          
        double totalCostPants;
        double totalCostPantspTax;
          //initial variable for the total cost of pants (no tax)
        totalCostPants = numPants * pantsPrice;
        totalCostPantspTax = totalCostPants;
          //the total cost of pants (pre tax)
        System.out.println(" ");
          //establishes one line space for organizational purposes
          
        System.out.println("The cost of the pants before tax is $" + totalCostPants);
          //prints out the total cost of the pants before tax is calculated
        double salesTaxPants;
          //initializes sales tax for pants variable
        salesTaxPants = totalCostPants * paSalesTax;
          //the sales tax collected on the purchase of pants
        salesTaxPants = (int) (salesTaxPants * 100) / (100.0);
          //converts to 2 decimal place number                                                                
        System.out.println("The sales tax on the purchase of pants is $" + salesTaxPants);
          //prints out the sales tax based on the pants purchase
        totalCostPants = totalCostPants + salesTaxPants;
          //adjusts the totalCostPants variable so that tax is now included
        totalCostPants = (int) (totalCostPants * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The total cost of the pants with tax is $" + totalCostPants);
          //prints out the final price of the pants with tax included
        System.out.println(" ");
          //extra line for organization
        
        double totalCostShirts;
        double totalCostShirtspTax;
          //initial variable for the total cost of shirts (no tax)
        totalCostShirts = numShirts * shirtPrice;
        totalCostShirtspTax = totalCostShirts;
          //the total cost of shirts (pre tax)
        System.out.println("The cost of the shirts before tax is $" + totalCostShirts);
          //prints out the total cost of the shirts before tax is calculated
        double salesTaxShirts;
          //initializes sales tax for shirts variable
        salesTaxShirts = totalCostShirts * paSalesTax;
          //the sales tax collected on the purchase of shirts
        salesTaxShirts = (int) (salesTaxShirts * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The sales tax on the purchase of shirts is $" + salesTaxShirts);
          //prints out the sales tax based on the shirts purchase
        totalCostShirts = totalCostShirts + salesTaxShirts;
          //adjusts the totalCostShirts variable so that tax is now included
        totalCostPants = (int) (totalCostPants * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The total cost of the shirts with tax is $" + totalCostShirts);
          //prints out the final price of the shirts with tax included  
        System.out.println(" ");
          //extra line for organization
        
        double totalCostBelts;
        double totalCostBeltspTax;
          //initial variable for the total cost of belts (no tax)
        totalCostBelts = numBelts * beltCost;
        totalCostBeltspTax = totalCostBelts;        
          //the total cost of belts (pre tax)
        System.out.println("The cost of the belts before tax is $" + totalCostBelts);
          //prints out the total cost of the belts before tax is calculated
        double salesTaxBelts;
          //initializes sales tax for belts variable
        salesTaxBelts = totalCostBelts * paSalesTax;
          //the sales tax collected on the purchase of belts
        salesTaxBelts = (int) (salesTaxBelts * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The sales tax on the purchase of belts is $" + salesTaxBelts);
          //prints out the sales tax based on the belts purchase
        totalCostBelts = totalCostBelts + salesTaxBelts;
          //adjusts the totalCostBelts variable so that tax is now included
        totalCostBelts = (int) (totalCostBelts * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The total cost of the belts with tax is $" + totalCostBelts);
          //prints out the final price of the belts with tax included
        System.out.println(" ");
          //extra line for organization
          
        double absoluteTotalCost;
          //this variable accommodates for all items purchased
        absoluteTotalCost = totalCostPantspTax + totalCostShirtspTax + totalCostBeltspTax;
          //adds the costs of all the different articles of clothing bought
        System.out.println("The total cost for all merchandise is $" + absoluteTotalCost);
          //prints out the total cost of all items pre tax
        double taxAbsolute;
          //represents the tax collected on the absolute purchase of all goods
        taxAbsolute = paSalesTax * absoluteTotalCost;
          //the total tax collected on the purchase of all items
        taxAbsolute = (int) (taxAbsolute * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The sales tax on the final total is $" + taxAbsolute);
          //prints the total tax collected om the purchase of all items
        System.out.println(" ");
          //extra line for organization
        
        absoluteTotalCost = absoluteTotalCost + taxAbsolute;
          //recalculates the total cost of all goods to accommodate for the total sales tax as calculated in lines prior
        absoluteTotalCost = (int) (absoluteTotalCost * 100) / (100.0);
          //converts to 2 decimal place number    
        System.out.println("The total cost with tax of everything is $" + absoluteTotalCost);
          //prints out the total transaction cost
          
          
          //END CODE
        }
}
