////////////////////
////WADE WARNER//////
/////12 - 4 - 2018 //
/////////////////////

import java.util.Scanner;

public class HW10 {
  
  public static void main (String [] args) {
    
    int [][] ticTac = new int[][] {{1,2,3},{4,5,6},{7,8,9}}; //XXO board array
    int [][] gameBoard = new int [3][3]; //game board XXO array
    
    printBoardInitial(ticTac); //print the initial array
    
    int ingame = 1; //1 if game is still on,  0 if not
    int playerOp = 1; //player 1 or player 2
    int turn = 1; //turn in game
    int choice = 0; //choice on board
    int flag = 0; //flag on choice
    
    int [] spot = new int [10]; //spot array to indcate whether choice has been chosen or not
    
    for (int a = 0; a < spot.length; a++) {
      spot[a] = -1; //spots that do not exist on board
    }
    
   
   
    while (ingame == 1){ //if in game
     playerOp = playerTurn(turn); //player
     choice = playerChoice(playerOp); //player's choice
     flag = flagChoice(choice,spot,turn); //flags choice, if necessary 
     if (flag >= 0) { //if not flagged
       gameBoard = gamePlay(ticTac, flag, playerOp); //gets a gameboard that is evaluated
       ingame = printBoardGame(gameBoard,playerOp,choice); //take a gameboard and evaluate if the game should continue or not
       turn++; //increase the turn for each time the game is not concluded
     }
     else {turn += 0;} //if the choice is flagged, the turn does not increase
     
     if (turn == 10) { //if the board gets filled without a clear victor
       break;
     }
    }
    if (turn == 10) { //when the board gets filled without a clear victor, or a draw
    System.out.println("Draw.");
    }
    
    else{
      System.out.println("Congrats player " + playerOp + ". You won!"); //prints who won
  }
  }
 public static void printBoardInitial (int [][] ticTac) { //method to print the board initially
   for (int i = 0; i < ticTac.length; i++) {
      for (int j = 0; j < ticTac.length; j++) {
         System.out.print(ticTac[i][j] + " "); //prints the board    
      }
      System.out.println();
    }
   System.out.println();
  }
  
  public static int playerTurn (int turn) { //method to determine who is up
    
    if (turn%2 == 0) { //if the turn is even, the second player goes, if odd, player 1
      return 2;
    }
    else {return 1;}
  }
  
  public static int playerChoice (int playerOp) { //method to get player's choice
    System.out.println("Player " + playerOp + ", it is your turn. Choose where you want to go."); //asks user for position
    Scanner scr = new Scanner (System.in);
    String junk;
    int choice;
  while (!scr.hasNextInt()) { //holds until integer is put in
      
      System.out.println("Non-integer. Re-enter: "); //asks for an integer as one was not inserted
      junk = scr.nextLine(); //junk string
    }
    
    choice = scr.nextInt(); //reads for the next integer
    junk = scr.nextLine(); //junk string
    
    return choice;
}

  public static int flagChoice (int choice, int [] spot, int turn){ //method to flag the choice if it has already been chosen
    int achieve = 0;
    for (int i = 0; i < spot.length; i++) {
      if (spot[i] == choice) {
        System.out.println("Place has been chosen. Choose again."); //indicates place has been chosen
        return -1; //flags value
       }
      else {
        achieve = choice; //achieved value, is choice
      }
  
  }
    spot[turn] = choice; //puts the chosen number in the spot array
    return achieve; //returns whether the place has been chosen or not
  }
  
  public static int [][] gamePlay (int [][] ticTac, int choice, int player) { //method to get the player's chosen spot into an array
    System.out.println("GAMEPLAY");
    if (player == 1) {
    switch (choice) { //choice based on player choice above
      case 1: //different cases
        ticTac[0][0] = 100;
        break;
      case 2:
        ticTac[0][1] = 100;
        break;
      case 3:
        ticTac[0][2] = 100;
        break;
      case 4:
        ticTac[1][0] = 100;
        break;        
      case 5:
        ticTac[1][1] = 100;
        break;
      case 6:
        ticTac[1][2] = 100;
        break;
      case 7:
        ticTac[2][0] = 100;
        break;
      case 8:
        ticTac[2][1] = 100;
        break;
      case 9:
        ticTac[2][2] = 100;
        break;   
    }
    }
    else {
    switch (choice) { //choice based on player choice above
      case 1: //different cases
        ticTac[0][0] = -100;
        break;
      case 2:
        ticTac[0][1] = -100;
        break;
      case 3:
        ticTac[0][2] = -100;
        break;
      case 4:
        ticTac[1][0] = -100;
        break;        
      case 5:
        ticTac[1][1] = -100;
        break;
      case 6:
        ticTac[1][2] = -100;
        break;
      case 7:
        ticTac[2][0] = -100;
        break;
      case 8:
        ticTac[2][1] = -100;
        break;
      case 9:
        ticTac[2][2] = -100;
        break;   
    }
    }
    
    return ticTac;
  }
  
  public static int printBoardGame (int [][] gameBoard, int player, int choice) { //method which prints the XXO board
    String [][] ticTacGame = new String [3][3]; //holder array for XXO Board
    
    for (int i = 0; i < ticTacGame.length; i++) {
      for (int j = 0; j < ticTacGame.length; j++) {
        if (gameBoard[i][j] == 100) { //100 to count for position for p.1
          ticTacGame[i][j] = "X"; //X for player 1
        }
        else if (gameBoard[i][j] == -100) { //-100 count for position for p.2
          ticTacGame[i][j] = "O"; //O for player 2
        }
        else {
        ticTacGame[i][j] = "" + gameBoard[i][j]; //converts int to string for compatibility
    }
        System.out.print(ticTacGame[i][j] + " "); //prints location of X and O on board
  }
      System.out.println();
}
    int ingame = orderCheck(ticTacGame); //whether game resumes or not, by checking array for input values
    return ingame; //temp, more methods will be included to change ingame regulator value
  }

  public static int orderCheck (String [][] ticTacGame) { //method to check for win combo for XXO
    int ingame = 0;
    int leftColX = 0;
    int midColX = 0;
    int rtColX = 0;
    int topRowX = 0;
    int midRowX = 0;
    int lowRowX = 0;
    int diagDownX = 0;
    int diagUpX = 0;
    int leftColO = 0;
    int midColO = 0;
    int rtColO = 0;
    int topRowO = 0;
    int midRowO = 0;
    int lowRowO = 0;
    int diagDownO = 0;
    int diagUpO = 0;
    
    if (ticTacGame[0][0].equals("O")) {  //1 position O
      leftColO++;
      topRowO++;
      diagDownO++;
    }
    if (ticTacGame[1][0].equals("O")) { //2 position O
      leftColO++;
      midRowO++;
    }
    if (ticTacGame[2][0].equals("O")) { //3 position O
      leftColO++;
      lowRowO++;
      diagUpO++;
    }
    if (ticTacGame[0][1].equals("O")) { //4 position O
      midColO++;
      topRowO++;
    }
    if (ticTacGame[1][1].equals("O")) { //5 position O
      midColO++;
      midRowO++;
      diagUpO++;
      diagDownO++;
    }
    if (ticTacGame[2][1].equals("O")) { //6 position O
      midColO++;
      lowRowO++;
    }
    if (ticTacGame[0][2].equals("O")) { //7 position O
      rtColO++;
      topRowO++;
      diagUpO++;
      }
    if (ticTacGame[1][2].equals("O")) { //8 position O
      rtColO++;
      midRowO++;
    }
    if (ticTacGame[2][2].equals("O")) { //9 position O
      rtColO++;
      lowRowO++;
      diagDownO++;
    }
  
    
    if (ticTacGame[0][0].equals("X")) {  //1 position X
      leftColX++;
      topRowX++;
      diagDownX++;
    }
    if (ticTacGame[1][0].equals("X")) { //2 position X
      leftColX++;
      midRowX++;
    }
    if (ticTacGame[2][0].equals("X")) { //3 position X
      leftColX++;
      lowRowX++;
      diagUpX++;
    }
    if (ticTacGame[0][1].equals("X")) { //4 position X
      midColX++;
      topRowX++;
    }
    if (ticTacGame[1][1].equals("X")) { //5 position X
      midColX++;
      midRowX++;
      diagUpX++;
      diagDownX++;
    }
    if (ticTacGame[2][1].equals("X")) { //6 position X
      midColX++;
      lowRowX++;
    }
    if (ticTacGame[0][2].equals("X")) { //7 position X
      rtColX++;
      topRowX++;
      diagUpX++;
      }
    if (ticTacGame[1][2].equals("X")) { //8 position X
      rtColX++;
      midRowX++;
    }
    if (ticTacGame[2][2].equals("X")) { //9 position X
      rtColX++;
      lowRowX++;
      diagDownX++;
    }
    
    if ((lowRowX == 3) || (midRowX == 3) || (topRowX == 3) || (leftColX == 3) || (midColX == 3) || (rtColX == 3) || (diagUpX == 3) || (diagDownX == 3)) { //IF ANY OF THE TIC-TAC-TOE CRITERIA ARE MET FOR X
    ingame = 0; //the game quits with winner
    }
    else if ((lowRowO == 3) || (midRowO == 3) || (topRowO == 3) || (leftColO == 3) || (midColO == 3) || (rtColO == 3) || (diagUpO == 3) || (diagDownO == 3)) { //IF ANY OF THE TIC-TAC-TOE CRITERIA ARE MET FOR O
    ingame = 0; //the game quits with winner
    }
  else { ingame = 1;} //if no solution met, then continue to game 
  return ingame;
}
}
  