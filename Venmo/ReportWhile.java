////////////
//// CSE 02 VenmoVariables
///  Wade Warner, Tommy Persaud, ODILON B. NIYOMUGABO
///  Group G
///  Section 210
///  10/1/2018

import java.lang.Math;
import java.util.Scanner;

public class ReportWhile {
  public static void main(String args[]) {

    Scanner scnr = new Scanner(System.in);

    double amountTransacted = 0;
    double amount = 0;
    System.out.println("Number of transactions: ");
    int numTransactions = scnr.nextInt();
    double highestValue = 0;
    int count = 0;

    while (numTransactions > 0) {
    
      System.out.println("Enter amount transacted: ");
      amount = scnr.nextDouble();
        if (amount > highestValue){
          highestValue = amount;
        }
        else {highestValue = highestValue;}

      amountTransacted = amountTransacted + amount;
      numTransactions -= 1;
      count++;
    }
    double averageTransacted = (amountTransacted/count);
    System.out.println(" ");
    
    System.out.println("Max transaction amount: $" + highestValue);
    System.out.println("Total transaction amount: $" + amountTransacted);
    System.out.println("Average transacted: $" + averageTransacted);
  }

}