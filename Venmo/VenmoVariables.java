////////////
//// CSE 02 VenmoVariables
///  Wade Warner, Tommy Persaud, ODILON B. NIYOMUGABO
///  Group G
///  Section 210
///  9/4/2018

public class VenmoVariables{
  
  public static void main(String args[]){
    
    double balance = 100; //balance in account
    int bankAccountNum = 55555555; //number of our bank account
    double transAmount = 50; //the amount transferred
    int acctAccessNum = 1111; //the account/PIN number to access the account
    double transferFee = 1.75; //the transfer fee to deposit into an account
    double transPercent = .1; //transfer percentage
    double finalBalance; //the final balance after all transactions
    double updatedBalance; //the updated balance after all is done
    
    updatedBalance = balance - transAmount;
    // in this case, this is the removal of how much we transfer from our account from our balance
    
    finalBalance = balance - transferFee;
    // final balance is the established balance with the transfer fee taken awawy
  }
}
