////////////
//// CSE 02 VenmoVariables
///  Wade Warner, Tommy Persaud, ODILON B. NIYOMUGABO
///  Group G
///  Section 210
///  10/8/2018

import java.lang.Math;
import java.util.Scanner;

public class VenmoInterface {
  public static void main(String args[]) {

Scanner scr = new Scanner(System.in);

//int choice = 0; //choice will hold the users selection/input
    // input-- what variables will be needed?
    
String junk;
int setting = 0;

int acctPIN; //account number
double bnkAcct = 1000; //initial amount in bank account
double sendAmt; //amount to be sent
String recName; //recipient
double realbnkAcct;    
double reqAmt;
String reqName;    
    
    
    //So far, the do-while loop simply prints out the menu options. What else needs to happen here?
    do{
      int choice = 0;
      System.out.println("VENMO");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
      
       if(choice == 1){
        
        System.out.println("You have " + bnkAcct + " in your Venmo account.");

        System.out.println("How much would you like to send? : $");

          while (!scr.hasNextDouble()){

            System.out.println("ERROR: Need proper input");

            junk = scr.nextLine();
          }

          sendAmt = scr.nextDouble();
          junk = scr.nextLine();

          bnkAcct -= sendAmt;

          System.out.println("Who would you like to send this to?");

          while (scr.hasNextInt() || scr.hasNextDouble()){

            System.out.println("ERROR: Need proper input");

          }

          recName = scr.nextLine();

        System.out.println("You have sent $" + sendAmt + " to " + recName);

      }

        else if (choice == 2) {

          System.out.println("How much money do you want?: $");

        while (!scr.hasNextDouble()){

            System.out.println("ERROR: Need proper input");

            junk = scr.nextLine();
          }

          reqAmt = scr.nextDouble();
          junk = scr.nextLine();

        System.out.println("Who are you requesting this amount from?");

        while (scr.hasNextInt() || scr.hasNextDouble()){

            System.out.println("ERROR: Need proper input");

          }

          reqName = scr.nextLine();

          System.out.println("You have requested $" + reqAmt + " from " + reqName);
        }

        else if (choice == 3) {
          System.out.println("Your account balance is $" + bnkAcct);
        }         

        else if (choice == 4) {

          System.out.println("Quit");


        }
      
      
      

    }while(choice != 4);
  }
}
    