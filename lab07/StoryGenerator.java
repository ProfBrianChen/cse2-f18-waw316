////////////////////////
/////////WMW////////////
//////10/24/2018////////
////////////////////////

import java.util.Scanner;
import java.util.Random;
import java.lang.Math;

public class StoryGenerator{
  public static void main(String [] args){
    Scanner scr = new Scanner(System.in);
    String adj1, adj2, adj3;  //adjective strings
    String subj; //subject noun string
    String ptverb, ptverb2, ptverb3; //past-tense verb strings
    String obj, obj2, obj3; //object noun strings
    String memNoun; //the memory noun, which takes the first subject and modifies it for a following sentence

    String word = "Aa"; //initializing string word
      
    adj1 = Adjective(word); //grabs an adjective from the adjective method and sets it as the variable
    subj = SubjNoun(word); //grabs a subject noun from the subject noun method 
    ptverb = PastTenseVerb(word); //grabs a past tense verb from the pt verb method
    ptverb2 = PastTenseVerb(word); //grabs a past tense verb from the pt verb method
    ptverb3 = PastTenseVerb(word); //grabs a past tense verb from the pt verb method
    obj = ObjNoun(word); //grabs an object noun from the obj noun method
    obj2 = ObjNoun(word); //grabs an object noun from the obj noun method
    obj3 = ObjNoun(word); //grabs an object noun from the obj noun method
    adj2 = Adjective(word); //grabs an adjective from the adjective method
    adj3 = Adjective(word); //grabs an adjective from the adjective method
    
    System.out.println(adj1); //prints out the adjective
    System.out.println(subj); //prints out the subject (1st) noun
    System.out.println(ptverb); //prints out the past tense verb
    System.out.println(obj); //prints out the object (2nd) noun
    System.out.println(adj2); //prints out the second adjective
    
    System.out.println("The " + adj1 + " " + subj + " " + ptverb + " the " + adj2 + " " + obj + "."); 
    //prints out the first sentence setup
    
    System.out.println();
    
    memNoun = MemoryNoun(subj); //grabs the way the primary noun should be expressed for the second sentence
    
    System.out.println("The " + adj1 + " " + subj + " " + ptverb + " the " + adj2 + " " + obj + ".");
    //prints out the first sentence setup
    System.out.println(memNoun + " " + ptverb2 + " the " + obj2 + " with the " + adj3 + " " + obj3 + ".");
    //prints out the second sentence setup
    System.out.println("The " + subj + " " + ptverb3 + " its " + obj3 + ".");
    //prints out the final sentence setup
  }
    public static String Adjective (String word) { //adjective method
      
      int rand = (int) (Math.random()*9); //provides a random number
      switch(rand) { //for the random variable, each case follows
        case 0:
          word = "young"; //the choice word, same setup for each case
          break;
        case 1:
          word = "orange";
          break;
        case 2:
          word = "big";
          break;
        case 3:
          word = "nice";
          break;
        case 4:
          word = "crazy";
          break;
        case 5:
          word = "blue";
          break;
        case 6:
          word = "wild";
          break;
        case 7:
          word = "sad";
          break;
        case 8:
          word = "lonely";
          break;
        case 9:
          word = "tired";
          break;
      }
      return word;//returns the word selected
    }
    
   public static String SubjNoun (String word) { //subject noun method
      
      int rand = (int) (Math.random()*9); //provides a random number
      switch(rand) { //for the random variable, each case follows
        case 0:
          word = "dog"; //the choice word, same setup for each case
          break;
        case 1:
          word = "brick";
          break;
        case 2:
          word = "tree";
          break;
        case 3:
          word = "fruit";
          break;
        case 4:
          word = "man";
          break;
        case 5:
          word = "item";
          break;
        case 6:
          word = "car";
          break;
        case 7:
          word = "city";
          break;
        case 8:
          word = "light";
          break;
        case 9:
          word = "road";
          break;
      }
      return word;
    }

   public static String PastTenseVerb (String word) { //past tense verb method
      
      int rand = (int) (Math.random()*9); //provides a random number
      switch(rand) { //for the random variable, each case follows
        case 0:
          word = "ran"; //the choice word, same setup for each case
          break;
        case 1:
          word = "wrote";
          break;
        case 2:
          word = "looked";
          break;
        case 3:
          word = "saw";
          break;
        case 4:
          word = "ate";
          break;
        case 5:
          word = "hit";
          break;
        case 6:
          word = "walked";
          break;
        case 7:
          word = "pushed";
          break;
        case 8:
          word = "exited";
          break;
        case 9:
          word = "twisted";
          break;
      }
      return word;//returns the word selected
    }
   
   public static String ObjNoun (String word) { //object noun method
      
      int rand = (int) (Math.random()*9); //provides a random number
      switch(rand) { //for the random variable, each case follows
        case 0:
          word = "dog"; //the choice word, same setup for each case
          break;
        case 1:
          word = "brick";
          break;
        case 2:
          word = "tree";
          break;
        case 3:
          word = "fruit";
          break;
        case 4:
          word = "man";
          break;
        case 5:
          word = "item";
          break;
        case 6:
          word = "car";
          break;
        case 7:
          word = "city";
          break;
        case 8:
          word = "light";
          break;
        case 9:
          word = "road";
          break;
      }
      return word;//returns the word selected
    }
   
   public static String MemoryNoun (String subj) {//method for setup for remembering the primary noun for the second sentence
      
     int rand = (int) (Math.random()); //random number of 0 or 1
      switch (rand) { //switch statement to identify what the replacement will be
        case 0:
        subj = "The " + subj; //the plus the subject of the word
        break;
        case 1:
        subj = "It"; //the subject is replaced entirely by it
        break;
      }
      
      return subj; //returns the randomly chosen choice
   }
}
      
      