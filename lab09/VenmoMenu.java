import java.util.Scanner; //scanner import
 
public class VenmoMenu{ //main method
  public static void main(String [] args){
    Scanner input = new Scanner(System.in); //brings in scanner 
    String [] friends = new String [5]; //array of friends
    double [] moneySent = new double [friends.length]; //array of money sent according to friend
    double [] moneyGot = new double [friends.length]; //array of monet got according to friend
    int choice; //menu choice
    int countInitFriends = 0; //intial number of friends, necessary for when adding new friends
    double balance = 100; //balance, intial at first, but subject to change
    double money = 0; //money sent or got
    double moneySend = 0; //the money sent, important for array moneySent
    double moneyGet = 0; //the money sent, important for array moneyGot
    String friend; //input for friend
    int friendIndex = 0; //friend index, important for money Arrays
    
    System.out.println("How many friends do you want to add to your list?: "); //asks user for # of friends
    int friendlst = input.nextInt(); //input #
    input.nextLine();//clears buffer
    for (int j = 0; j < friendlst; j++) {//for the number of friends provided
    System.out.println("Friend: ");
    friends[j] = input.nextLine();//inputs friend into friends array
    countInitFriends++; //counts the number of friends input
    }
    for (int k = friends.length - 1; k > friendlst - 1; k--) { //for all remaining spots, we put in a holder string (in this case, username ///, which is thus restricted)
    friends[k] = "///";
    }
    do{
      printMenu(); //runs the printMenu method
      choice = getInt(); //gets the integer for the menu choice
      switch (choice){ //switch setup to run different menu options
        case 1: getBalance(balance); //gets balance from balance method
                friendIndex = getFriend(friends); //determines what position a certain friend is, if eligiible, in friends index
                if (friendIndex < 0) { //if the friend is not in the list
                  break; //break out of switch
                }
                moneySend = sendMoney(balance,friends,friendIndex); //money anticipated to be sent
                balance -= moneySend; //takes money sent away from balance
                moneySent[friendIndex] += moneySend; //money sent per person
                break;
        case 2: getBalance(balance); //gets balance
                friendIndex = getFriend(friends); //gets friend index
                if (friendIndex < 0) { //if the friend is not in the list
                  break; //break out of switch
                }
                moneyGet = requestMoney(balance, friends, friendIndex); //gets money to request
                balance += moneyGet; //adds to balance
                moneyGot[friendIndex] += moneyGet; //adds money to get from
                break;
        case 3: getBalance(balance); //reports balance
                break;
        case 4: friends = addFriends(friends,countInitFriends); //adds friends with addFriends method
                break;
        case 5: printVenmoReport(friends,moneySent,moneyGot); //prints a report based on friends
                break;
        case 6: System.out.println("Goodbye"); //exit
                break;
        default: System.out.println("you entered an invalid value -- try again"); //invalid
                break;
      }
    }
    while(choice != 6); //while the choice is not an exit choice
  }
  
  
  //print Venmo Main Menu
  public static void printMenu(){
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Add Friends");
      System.out.println("5. Print Transaction Report");
      System.out.println("6. Quit");
  }
  
  //get a number and check for an integer
  public static int getInt(){
    Scanner input = new Scanner(System.in);
     while(input.hasNextInt() == false){
        System.out.println("You entered and invalid value -- try again");
        input.nextLine();//clear buffer
     }
     return input.nextInt();
  } 
  public static void getBalance(double balance){
     System.out.println("You have $" + balance + " in your account" );
  }
  
  //get a number and check for a double
  public static double getDouble(){
    Scanner input = new Scanner(System.in);
    while(input.hasNextDouble() == false){
         System.out.println("You entered and invalid value -- try again");
         input.nextLine(); //clear buffer
     } 
     return input.nextDouble();
  }
  
  //driver method for sendMoney //MODIFIED TO WORK
  public static double sendMoney(double balance, String [] friends, int friend){
      double startBalance = balance; //balance before any sending of funds
      double money = 0; //money initial 
      int transType = -1; //transaction type, +1 for request, -1 for sending out
      System.out.println("How much money do you want to send?");
      money = getDouble();//get and check user input for a double
      balance = checkMoney(money,balance,transType); //check valid amount no negative and if enough money is in balance to send money 
      if (balance == startBalance) { //if the newly calculated balance is the same as before
        System.out.println("Due to an input error, your request could not be processed.");
      }
      else {
        System.out.println("Transaction complete: You have sent $" + money + " to " + friends[friend]);
        System.out.println("Once confirmed, your new balance will be $" + balance);
      }
      
      return money; //returns the amount of money sent
  }
  
  //method prints friend and money sent to friend
  public static int getFriend(String [] names){
        String friend; //friend's name string
        int friendcheck; //friend check, an index value
        Scanner input = new Scanner(System.in); //import scanner
        System.out.println("Enter friend's name: ");     
        friend = input.nextLine(); //stores friends name
        friendcheck = listSearch(names, friend); //checks to see if friend is on names array, return of type int
        if (friendcheck == -1){ //friend is not in array
        while (friendcheck == -1) {
          System.out.println("Friend is not in friends list: Please re-enter all pertinent information.");
          System.out.println("Enter friend's name: ");    
          friend = input.nextLine();
          friendcheck = listSearch(names, friend);
          //rechecks for new friend input
        }
        }
        else {
        System.out.println("Friend found: Please continue"); //friend found
        }
        return friendcheck; //returns if friend found and at what position
        }
  
  /*method that makes sure money entered is greater than 0, 
   * there are sufficient funds in balance, if sufficient funds the balance is changed
   * and returned. if funds are not sufficient the user is taken back to the main menu
   */
  public static double checkMoney(double money, double balance, int transType) {
        while (money <= 0){
            System.out.println("Invalid amount entered");
            money = getDouble();
         }
        
        if (transType < 0){
         if (money <= balance){
             balance -= money;
             }
          else{
             System.out.println("You do not have sufficient funds for this transaction.");
          }
        }
        if (transType > 0) {
          if (money <= balance) {
            balance += money;
          }
          else{
            System.out.println("You do not have sufficient funds for this transaction.");
          }
        }
          return balance;
  }
  
  //driver method for requestMonday
  public static double requestMoney(double balance, String [] friends, int friend){
    double startBalance = balance; //the balance prior to any change based on request 
    int transType = 1;//transaction type, +1 for request, -1 for sending out
    System.out.println("How much money do you want to request?");
     double money = getDouble(); //gets money requested
     balance = checkMoney(money, balance, transType); //runs the money through the checkMoney method
     if (balance == startBalance) {//if the newly calculated balance is the same as before
       System.out.println("Due to an input error, your request could not be processed.");
     }
     else {
     System.out.println("Once confirmed, your new balance will be $" + balance);
     }
     return money; //returns money requested
               
}
  
  //gets friends information for the Request menu item and prints the amount requested from friend
  public static int getFriendRequest(double money, String [] names){
     int friendcheck;
     Scanner input = new Scanner(System.in);
     System.out.println("Who do you want to request money from?");
     String friend = input.nextLine();
     friendcheck = listSearch(names, friend);
     if (friendcheck == -1) {
          while (friendcheck == -1) {
          System.out.println("Friend is not in friends list: Please re-enter all pertinent information.");
          System.out.println("Enter friend's name: ");    
          friend = input.nextLine();
          friendcheck = listSearch(names, friend);
        }
      }
     else {
     System.out.println("You are requesting $" + money + " from "+ friend);
     }
     return friendcheck;
     }
  
  
  //method makes sure money is greater than 0 and adds requested amount to balance and returns new balance
  public static double checkRequestMoney(double money, double balance, String [] names){
      while (money <= 0){
           System.out.println("Invalid amount entered");
           money = getDouble();
      }
      balance += money;
      int eligible = getFriendRequest(money,names);
      if (eligible == -1) {
        balance -= money;
      }
      return eligible;
  }
  
  public static String [] addFriends (String [] friends, int numFriends) {
    String junk;
    Scanner input = new Scanner (System.in);
    System.out.println("How many friends do you want to add? Due to system limitations, you can only enter " + (friends.length - numFriends) + " new friends.");
    int addFriends = input.nextInt();
    
    while (addFriends > (friends.length - numFriends)) {
      
      System.out.println("Due to system limitations, you can only enter " + (friends.length - numFriends) + " new friends.");
      addFriends = input.nextInt();
     
    }
         junk = input.nextLine();  
           for (int t = numFriends; t < (numFriends + addFriends) ; t++) {
             System.out.println("Friend: ");
             friends[t] = input.nextLine();
           }
           System.out.println("List done");
    return friends;
  }
  
  public static int listSearch (String [] names, String friend) {
    int s = 0;
    int errCount = 0;
    for (int i = 0; i < names.length; i++) {
      if (friend.equals("///")) {
        System.out.println("RESERVED KEYWORD");
        s = -1;
        break;
        //reserved keyword, not allowable as a username
      }
      else if (names[i].equals(friend)) {
        s = i;
        break;
        //if friend occurs in the array, s is the index value
      }
      errCount++;
    }
    if (errCount == names.length) {
        System.out.println("Friend not found.");
        s = -1;
        //otherwise, the friend is not in the list
    }
    
  return s;
  }
  
  public static void printVenmoReport (String [] friends, double [] moneySent, double [] moneyGet) {
    System.out.println("Name      Money Sent To      Money Requested");
    for (int i = 0; i < friends.length; i++) {
    
      System.out.print(friends[i] + "            " + moneySent[i] + "             " + moneyGet[i]);
      System.out.println();
    //prints out the report
    }
  }
}
    
        