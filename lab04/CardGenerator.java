////////////////////
////////// CSE 2 ///
//// WADE WARNER ///
//// LAB 04 ////////
//// 9/26/2018 /////
////////////////////

public class CardGenerator{
//main method required for every Java program

  public static void main (String [] args) {
  //end of main method
  
  int card = (int)(Math.random() * 52) + 1;
  //chooses a random card from a deck of 52
  
  int cardRem = card%4;
    //finds the remainder of cards are divided by the number of cards to get suit
  String suit;
    //what suit
    if (cardRem == 0) {
      suit = "Diamonds";
      //initializes the suit
    }
    else if (cardRem == 1) {
      suit = "Spades";
      //initializes the suit
    }
    else if (cardRem == 2) {
      suit = "Jacks";
      //initializes the suit
    }
    else{
      suit = "Hearts";
      //initializes the suit
    }
    
    int cardRem2 = card%13;
    
    switch (cardRem2) {
      case 0:
        System.out.println("You picked the Ace of " + suit);
        break;
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
        System.out.println("You picked the " + (cardRem2+1) + " of " + suit);
        break;
      case 10:
        System.out.println("You picked the Jack of " + suit);
        break;
      case 11:
        System.out.println("You picked the Queen of " + suit);
        break;
      case 12:
        System.out.println("You picked the King of " + suit);
          break;
    }
    
  }
  
 
  }
